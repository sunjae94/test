﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitTest
{
    class Person
    {
        string name;
        int age;

        public void init(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public void printPerson()
        {
            Console.Write(name + " " + age);
        }

        public void foo()
        {
            Console.Write("foo");
        }
    }
}
